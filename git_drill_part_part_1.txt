1 Create a directory called git_sample_project and cd to git_sample_project

PS C:\Users\Anu Gowda> mkdir git_sample_project                                                                                  

    Directory: C:\Users\Anu Gowda


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         7/8/2020  10:03 PM                git_sample_project


PS C:\Users\Anu Gowda> cd git_sample_project  



2.Initialize a git repo.

git init 
Initialized empty Git repository in C:/Users/Anu Gowda/git_sample_project/.git/


3. Create the following files a.txt, b.txt, c.txt 

mkdir a.txt
mkdir b.txt
mkdir c.txt

4 . Add some arbitrary content to the above files.

PS C:\Users\Anu Gowda\git_sample_project\a.txt> add-content a.txt "mountblue drill homework"                                     
PS C:\Users\Anu Gowda\git_sample_project\a.txt> add-content b.txt "happy to code" 

5. Add a.txt and b.txt to the staging area

git status
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        a.txt/
        b.txt/

nothing added to commit but untracked files present (use "git add" to track)

git add a.txt b.txt

6.Run git status. Understand what it says.
git status 
No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   a.txt/a.txt
        new file:   b.txt/b.txt
[the 2 files are added to the staging area]

7. Commit a.txt and b.txt with the message "Add a.txt and b.txt"

git commit -m "Add a.txt and b.txt"                                                    
[master (root-commit) e435ec5] 
Add a.txt and b.txt
 2 files changed, 2 insertions(+)
 create mode 100644 a.txt/a.txt
 create mode 100644 b.txt/b.txt

8 . Run git status. Understand what it says

git status                                                                             
On branch master
nothing to commit, working tree clean

9. Run git log

git log
commit e435ec58729839e5db8e53c935f9dfa51ff55d42 (HEAD -> master)
Author: srikantagowda <srikantagowda07@gmail.com> 
Date:   Wed Jul 8 22:15:36 2020 +0530
Add a.txt and b.txt


10 .Add and Commit c.txt

add-content c.txt "last add & commit content"
git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        ./
nothing added to commit but untracked files present (use "git add" to track)
PS C:\Users\Anu Gowda\git_sample_project\c.txt> ls                                                                               

    Directory: C:\Users\Anu Gowda\git_sample_project\c.txt

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----         7/8/2020  10:24 PM             27 c.txt

git add c.txt                                                                   
git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   c.txt
git commit -m "the second commit task as per the drill"
[master 48f6d9c] the second commit task as per the drill
 1 file changed, 1 insertion(+)
 create mode 100644 c.txt/c.txt
git status
On branch master
nothing to commit, working tree clean



11. Create a project on GitLab.
git config --global user.name "srikanta gowda"
git config --global user.email "srikanta.gowda@mountblue.tech"
 git init Initialized empty Git repository in C:/Users/Anu Gowda/git_sampleproject/.git/
git remote add origin https://gitlab.com/srikanta_gowda/git_project.git

 git status
 On branch master
No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        a.txt/
        b.txt/
        c.txt/

nothing added to commit but untracked files present (use "git add" to track)
git add .
git status
 On branch master
No commits yet
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   a.txt/a.txt
        new file:   b.txt/b.txt
        new file:   c.txt/c.txt



12 .Push your code to GitLab 
git commit -m "Initial commit" 
 [master (root-commit) 99ddce8] Initial commit
 3 files changed, 3 insertions(+)
 create mode 100644 a.txt/a.txt
 create mode 100644 b.txt/b.txt
 create mode 100644 c.txt/c.txt

git push -u origin master  


Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 12 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (8/8), 479 bytes | 79.00 KiB/s, done.
Total 8 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/srikanta_gowda/git_project.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.


13. Clone your project in another directory called git_sample_project_2
 mkdir git_sample_project_2                                                                

    Directory: C:\Users\Anu Gowda


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         7/9/2020  12:31 AM                git_sample_project_2

cd git_sample_project_2 
git clone https://gitlab.com/srikanta_gowda/git_project.git
Cloning into 'git_project'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 8 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (8/8), done.

14. In git_sample_project_2, add a file called d.txt



mkdir d.txt                                              

Directory: C:\Users\Anu Gowda\git_sample_project_2\git_project


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         7/9/2020  12:35 AM                d.txt



15 . Commit and push d.txt

git add d.txt
git commit -m "secound commit"



16. cd back to git_sample_project

cd..
cd git_sampleproject
Directory: C:\Users\Anu Gowda\git_sampleproject
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         7/8/2020  10:11 PM                a.txt
d-----         7/8/2020  10:11 PM                b.txt
d-----         7/8/2020  10:24 PM                c.txt


17. Pull the changes from GitLab
git push -u origin master

18 .Copy git_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab
done

19 . Copy cli_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab
done 
PS C:\Users\Anu Gowda\git_sampleproject> git push -u origin master
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 12 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 2.00 KiB | 682.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/srikanta_gowda/git_project.git
   99ddce8..1ba471f  master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
PS C:\Users\Anu Gowda\git_sampleproject>

20 .Add your mentor as a "Developer" to your project
made public 





